﻿namespace MushroomWeekends_Roulette
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.label1 = new System.Windows.Forms.Label();
            this.randomiseGameButton = new System.Windows.Forms.Button();
            this.loadGameListButton = new System.Windows.Forms.Button();
            this.wheelDecide = new System.Windows.Forms.WebBrowser();
            this.gameListBox = new System.Windows.Forms.DataGridView();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.progressLoading = new System.Windows.Forms.ToolStripProgressBar();
            this.statusLoading = new System.Windows.Forms.ToolStripStatusLabel();
            this.onlyInstalledBox = new System.Windows.Forms.CheckBox();
            this.customNames = new System.Windows.Forms.CheckBox();
            this.vieweditButton = new System.Windows.Forms.Button();
            this.totalLabel = new System.Windows.Forms.Label();
            this.totalgamesLabel = new System.Windows.Forms.Label();
            this.remaftchBox = new System.Windows.Forms.CheckBox();
            this.rpsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gameListBox)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Players SteamID";
            // 
            // randomiseGameButton
            // 
            this.randomiseGameButton.Enabled = false;
            this.randomiseGameButton.Location = new System.Drawing.Point(174, 265);
            this.randomiseGameButton.Name = "randomiseGameButton";
            this.randomiseGameButton.Size = new System.Drawing.Size(75, 23);
            this.randomiseGameButton.TabIndex = 4;
            this.randomiseGameButton.Text = "Randomise";
            this.randomiseGameButton.UseVisualStyleBackColor = true;
            this.randomiseGameButton.Click += new System.EventHandler(this.randomiseGameButton_Click);
            // 
            // loadGameListButton
            // 
            this.loadGameListButton.Location = new System.Drawing.Point(12, 265);
            this.loadGameListButton.Name = "loadGameListButton";
            this.loadGameListButton.Size = new System.Drawing.Size(75, 23);
            this.loadGameListButton.TabIndex = 3;
            this.loadGameListButton.Text = "Load";
            this.loadGameListButton.UseVisualStyleBackColor = true;
            this.loadGameListButton.Click += new System.EventHandler(this.loadGameListButton_Click);
            // 
            // wheelDecide
            // 
            this.wheelDecide.AllowWebBrowserDrop = false;
            this.wheelDecide.IsWebBrowserContextMenuEnabled = false;
            this.wheelDecide.Location = new System.Drawing.Point(253, 9);
            this.wheelDecide.MinimumSize = new System.Drawing.Size(20, 20);
            this.wheelDecide.Name = "wheelDecide";
            this.wheelDecide.ScrollBarsEnabled = false;
            this.wheelDecide.Size = new System.Drawing.Size(250, 250);
            this.wheelDecide.TabIndex = 9;
            this.wheelDecide.TabStop = false;
            this.wheelDecide.WebBrowserShortcutsEnabled = false;
            // 
            // gameListBox
            // 
            this.gameListBox.AllowUserToResizeColumns = false;
            this.gameListBox.AllowUserToResizeRows = false;
            this.gameListBox.BackgroundColor = System.Drawing.Color.White;
            this.gameListBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gameListBox.Location = new System.Drawing.Point(12, 51);
            this.gameListBox.Name = "gameListBox";
            this.gameListBox.RowHeadersVisible = false;
            this.gameListBox.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gameListBox.Size = new System.Drawing.Size(237, 167);
            this.gameListBox.TabIndex = 5;
            this.gameListBox.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.gameListBox_RowsAdded);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.progressLoading,
            this.statusLoading});
            this.statusStrip.Location = new System.Drawing.Point(0, 291);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip.Size = new System.Drawing.Size(512, 22);
            this.statusStrip.TabIndex = 11;
            this.statusStrip.Text = "statusStrip1";
            // 
            // progressLoading
            // 
            this.progressLoading.Name = "progressLoading";
            this.progressLoading.Size = new System.Drawing.Size(100, 16);
            // 
            // statusLoading
            // 
            this.statusLoading.Name = "statusLoading";
            this.statusLoading.Size = new System.Drawing.Size(82, 17);
            this.statusLoading.Text = "Ready to work";
            // 
            // onlyInstalledBox
            // 
            this.onlyInstalledBox.AutoSize = true;
            this.onlyInstalledBox.Location = new System.Drawing.Point(12, 242);
            this.onlyInstalledBox.Name = "onlyInstalledBox";
            this.onlyInstalledBox.Size = new System.Drawing.Size(88, 17);
            this.onlyInstalledBox.TabIndex = 6;
            this.onlyInstalledBox.Text = "Only installed";
            this.onlyInstalledBox.UseVisualStyleBackColor = true;
            // 
            // customNames
            // 
            this.customNames.AutoSize = true;
            this.customNames.Location = new System.Drawing.Point(122, 242);
            this.customNames.Name = "customNames";
            this.customNames.Size = new System.Drawing.Size(95, 17);
            this.customNames.TabIndex = 7;
            this.customNames.Text = "Custom names";
            this.customNames.UseVisualStyleBackColor = true;
            this.customNames.CheckedChanged += new System.EventHandler(this.customNames_CheckedChanged);
            // 
            // vieweditButton
            // 
            this.vieweditButton.Location = new System.Drawing.Point(12, 22);
            this.vieweditButton.Name = "vieweditButton";
            this.vieweditButton.Size = new System.Drawing.Size(101, 23);
            this.vieweditButton.TabIndex = 13;
            this.vieweditButton.Text = "View\\Edit List";
            this.vieweditButton.UseVisualStyleBackColor = true;
            this.vieweditButton.Click += new System.EventHandler(this.vieweditButton_Click);
            // 
            // totalLabel
            // 
            this.totalLabel.AutoSize = true;
            this.totalLabel.Location = new System.Drawing.Point(119, 27);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(79, 13);
            this.totalLabel.TabIndex = 14;
            this.totalLabel.Text = "Total players: 0";
            // 
            // totalgamesLabel
            // 
            this.totalgamesLabel.AutoSize = true;
            this.totalgamesLabel.Location = new System.Drawing.Point(12, 221);
            this.totalgamesLabel.Name = "totalgamesLabel";
            this.totalgamesLabel.Size = new System.Drawing.Size(77, 13);
            this.totalgamesLabel.TabIndex = 15;
            this.totalgamesLabel.Text = "Total games: 0";
            // 
            // remaftchBox
            // 
            this.remaftchBox.AutoSize = true;
            this.remaftchBox.Location = new System.Drawing.Point(122, 221);
            this.remaftchBox.Name = "remaftchBox";
            this.remaftchBox.Size = new System.Drawing.Size(125, 17);
            this.remaftchBox.TabIndex = 16;
            this.remaftchBox.Text = "Remove after choice";
            this.remaftchBox.UseVisualStyleBackColor = true;
            this.remaftchBox.CheckedChanged += new System.EventHandler(this.remaftchBox_CheckedChanged);
            // 
            // rpsButton
            // 
            this.rpsButton.Location = new System.Drawing.Point(253, 265);
            this.rpsButton.Name = "rpsButton";
            this.rpsButton.Size = new System.Drawing.Size(250, 23);
            this.rpsButton.TabIndex = 17;
            this.rpsButton.Text = "Play rock–paper–scissors (battle for roll)\r\n";
            this.rpsButton.UseVisualStyleBackColor = true;
            this.rpsButton.Click += new System.EventHandler(this.rpsButton_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(512, 313);
            this.Controls.Add(this.rpsButton);
            this.Controls.Add(this.remaftchBox);
            this.Controls.Add(this.totalgamesLabel);
            this.Controls.Add(this.totalLabel);
            this.Controls.Add(this.vieweditButton);
            this.Controls.Add(this.customNames);
            this.Controls.Add(this.onlyInstalledBox);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.gameListBox);
            this.Controls.Add(this.wheelDecide);
            this.Controls.Add(this.loadGameListButton);
            this.Controls.Add(this.randomiseGameButton);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MushroomWeekends Roulette";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gameListBox)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button randomiseGameButton;
        private System.Windows.Forms.Button loadGameListButton;
        private System.Windows.Forms.WebBrowser wheelDecide;
        private System.Windows.Forms.DataGridView gameListBox;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripProgressBar progressLoading;
        private System.Windows.Forms.ToolStripStatusLabel statusLoading;
        private System.Windows.Forms.CheckBox onlyInstalledBox;
        private System.Windows.Forms.CheckBox customNames;
        private System.Windows.Forms.Button vieweditButton;
        private System.Windows.Forms.Label totalLabel;
        private System.Windows.Forms.Label totalgamesLabel;
        private System.Windows.Forms.CheckBox remaftchBox;
        private System.Windows.Forms.Button rpsButton;
    }
}

