﻿using System;

namespace MushroomWeekends_Roulette
{
    public class Game
    {

        public int AppId { get; set; }

        public String Name { get; set; }

        public Game()
        {
            AppId = 0;
            Name = "";
        }

    }
}
