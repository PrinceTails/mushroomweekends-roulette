﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MushroomWeekends_Roulette
{
    public partial class AddPlayerForm : Form
    {
        public List<Player> listFromForm;
        public List<Player> listFromSteam;
        Steam_Friends sf;
        public AddPlayerForm()
        {
            InitializeComponent();
            listFromForm = new List<Player>();
            listFromSteam = new List<Player>();
        }

        private void addButton_Click(object sender, EventArgs e)
        {

            addPlayer();
            //playersList.Items.Add(playerBox.Text);
        }
        private void addPlayer()
        {
            if (playerBox.Text == "")
                return;

            string url = "http://steamcommunity.com/id/" + playerBox.Text + "/?xml=1";

            var reader = XmlReader.Create(url, new XmlReaderSettings { DtdProcessing = DtdProcessing.Parse });
            string id64 = "";
            string steamId = "";
            bool full = false;
            bool changedUrl = false;
            try
            {
                while (reader.Read() && !full)
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            switch (reader.Name)
                            {
                                case "error":
                                    if (changedUrl)
                                    {
                                        MessageBox.Show("User not found.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return;
                                    }
                                    reader = XmlReader.Create("http://steamcommunity.com/profiles/" + playerBox.Text + "/?xml=1");
                                    changedUrl = true;
                                    break;
                                case "steamID64":
                                    reader.Read();
                                    id64 = reader.Value;
                                    break;
                                case "steamID":
                                    reader.Read();
                                    steamId = reader.Value;
                                    break;
                                case "visibilityState":
                                    reader.Read();
                                    if (reader.Value != "3")
                                    {
                                        MessageBox.Show("Profile is private.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return;
                                    }
                                    break;
                            }
                            break;
                    }
                }
                if (ExistProfile(id64))
                {
                    MessageBox.Show("This user in list.");
                    return;
                }

                listFromForm.Add(new Player(steamId, null, id64, false));

                playersList.Items.Add(listFromForm[listFromForm.Count - 1].Name);
                filterList();
                playerBox.Text = "";
            }
            catch (Exception)
            {

            }
        }

        private bool ExistProfile(string id64)
        {
            for (int i = 0; i < listFromForm.Count; i++)
                if (listFromForm[i].Id64 == id64)
                    return true;
            for (int i = 0; i < listFromSteam.Count; i++)
                if (listFromSteam[i].Id64 == id64)
                    return true;
            return false;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if(playersList.SelectedIndex<0)
            {
                MessageBox.Show("Select player for delete.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int index = playersList.SelectedIndex;
            if (listFromSteam.Count - 1 >= index)
                listFromSteam.RemoveAt(index);
            else
                listFromForm.RemoveAt(index - listFromSteam.Count);


            playersList.Items.RemoveAt(index);
            //if(playersList.SelectedIndex)

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void linkLabel1_MouseEnter(object sender, EventArgs e)
        {
            communityTip.Show("Steam community ID.\n(ex.http://steamcommunity.com/id/NICKNAME/\n id = NICKNAME)", linkLabel1);
        }

        private void linkLabel1_MouseLeave(object sender, EventArgs e)
        {
            communityTip.Hide(linkLabel1);
        }

        private void fsteamButton_Click(object sender, EventArgs e)
        {
            if(sf==null)
                sf = new Steam_Friends();
            if(sf.ShowDialog()==DialogResult.OK)
            {
                playersList.Items.Clear();
                if (listFromSteam.Count != 0)
                {
                    listFromSteam = new List<Player>();
                    listFromSteam = sf.checkedFriends;
                }
                else
                {
                    for (int i = 0; i < sf.checkedFriends.Count; i++)
                        listFromSteam.Add(sf.checkedFriends[i]);
                }
                filterList();

                for (int i = 0; i < listFromSteam.Count; i++)
                {
                    playersList.Items.Add(listFromSteam[i].Name);
                    
                }
                for (int i = 0; i < listFromForm.Count; i++)
                    playersList.Items.Add(listFromForm[i].Name);

                //this.DialogResult = DialogResult.OK;
            }
        }

        private void filterList()
        {
            List<Player> tmp = new List<Player>();

            for(int i=0;i<listFromForm.Count;i++)
            {
                bool match = false;
                for(int j=0; j<listFromSteam.Count;j++)
                {
                    if (listFromForm[i].Id64 == listFromSteam[j].Id64)
                    {
                        match = true;
                        break;
                    }  
                }
                if (!match)
                    tmp.Add(listFromForm[i]);
            }
            listFromForm = tmp;
        }

        private void AddPlayerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (sf != null)
            {
                sf.Close();
                sf.Dispose();
            }
        }

        private void playerBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                addPlayer();
            }
        }
    }
}
