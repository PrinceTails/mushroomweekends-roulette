﻿namespace MushroomWeekends_Roulette
{
    partial class Steam_Friends
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Steam_Friends));
            this.loadFriends = new System.Windows.Forms.Button();
            this.friendsGrid = new System.Windows.Forms.DataGridView();
            this.checkColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.imageColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.steamID = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.friendlistLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.friendsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // loadFriends
            // 
            this.loadFriends.Location = new System.Drawing.Point(118, 8);
            this.loadFriends.Name = "loadFriends";
            this.loadFriends.Size = new System.Drawing.Size(75, 23);
            this.loadFriends.TabIndex = 1;
            this.loadFriends.Text = "Load";
            this.loadFriends.UseVisualStyleBackColor = true;
            this.loadFriends.Click += new System.EventHandler(this.loadFriends_Click);
            // 
            // friendsGrid
            // 
            this.friendsGrid.AllowUserToAddRows = false;
            this.friendsGrid.AllowUserToDeleteRows = false;
            this.friendsGrid.AllowUserToResizeColumns = false;
            this.friendsGrid.AllowUserToResizeRows = false;
            this.friendsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.friendsGrid.BackgroundColor = System.Drawing.Color.White;
            this.friendsGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.friendsGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.friendsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.friendsGrid.ColumnHeadersVisible = false;
            this.friendsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.checkColumn,
            this.imageColumn,
            this.nameColumn});
            this.friendsGrid.Location = new System.Drawing.Point(0, 85);
            this.friendsGrid.MultiSelect = false;
            this.friendsGrid.Name = "friendsGrid";
            this.friendsGrid.RowHeadersVisible = false;
            this.friendsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.friendsGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.friendsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.friendsGrid.Size = new System.Drawing.Size(193, 196);
            this.friendsGrid.TabIndex = 3;
            this.friendsGrid.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.friendsGrid_RowsAdded);
            // 
            // checkColumn
            // 
            this.checkColumn.FillWeight = 20F;
            this.checkColumn.HeaderText = "Check";
            this.checkColumn.MinimumWidth = 10;
            this.checkColumn.Name = "checkColumn";
            this.checkColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.checkColumn.Width = 10;
            // 
            // imageColumn
            // 
            this.imageColumn.HeaderText = "Avatar";
            this.imageColumn.MinimumWidth = 30;
            this.imageColumn.Name = "imageColumn";
            this.imageColumn.ReadOnly = true;
            this.imageColumn.Width = 30;
            // 
            // nameColumn
            // 
            this.nameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameColumn.HeaderText = "Name";
            this.nameColumn.MaxInputLength = 32;
            this.nameColumn.Name = "nameColumn";
            this.nameColumn.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-1, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Steam Community ID";
            // 
            // steamID
            // 
            this.steamID.Location = new System.Drawing.Point(2, 29);
            this.steamID.Name = "steamID";
            this.steamID.Size = new System.Drawing.Size(100, 20);
            this.steamID.TabIndex = 0;
            this.steamID.KeyUp += new System.Windows.Forms.KeyEventHandler(this.steamID_KeyUp);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(118, 37);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Confirm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // friendlistLabel
            // 
            this.friendlistLabel.AutoSize = true;
            this.friendlistLabel.Location = new System.Drawing.Point(-1, 69);
            this.friendlistLabel.Name = "friendlistLabel";
            this.friendlistLabel.Size = new System.Drawing.Size(51, 13);
            this.friendlistLabel.TabIndex = 5;
            this.friendlistLabel.Text = "Friend-list";
            // 
            // Steam_Friends
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(193, 282);
            this.Controls.Add(this.friendlistLabel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.steamID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.friendsGrid);
            this.Controls.Add(this.loadFriends);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Steam_Friends";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Steam Friends";
            ((System.ComponentModel.ISupportInitialize)(this.friendsGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button loadFriends;
        private System.Windows.Forms.DataGridView friendsGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox steamID;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn checkColumn;
        private System.Windows.Forms.DataGridViewImageColumn imageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameColumn;
        private System.Windows.Forms.Label friendlistLabel;
    }
}