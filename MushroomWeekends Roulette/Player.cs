﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;

namespace MushroomWeekends_Roulette
{
    public class Player
    {
        public List<Game> ListOfGames { get; set; }

        public String Name { get; set; }

        public Bitmap Image { get; set; }

        public String Id64 { get; set; }
        public bool From_steam { get; set; }

        public Player()
        {
            ListOfGames = new List<Game>();
        }
        public Player(String name, Bitmap img, string id, bool f)
        {
            Name = name;
            Image = img;
            Id64 = id;
            From_steam = f;
            ListOfGames = new List<Game>();
        }

        public void ParseData(String url)
        {
            try
            {
                ListOfGames = new List<Game>();
                var reader = XmlReader.Create(url, new XmlReaderSettings { DtdProcessing = DtdProcessing.Parse });

                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            switch (reader.Name)
                            {
                                case "game":
                                    Game tmp = new Game();
                                    reader.Read();
                                    reader.Read();
                                    reader.Read();
                                    tmp.AppId = Convert.ToInt32(reader.Value);
                                    reader.Read();
                                    reader.Read();
                                    reader.Read();
                                    reader.Read();
                                    tmp.Name = reader.Value;
                                    ListOfGames.Add(tmp);
                                    break;
                            }
                            break;

                        case XmlNodeType.EndElement:
                            if (reader.Name == "games")
                                return;
                            break;
                    }
                }
            }
            catch(Exception )
            {
                //throw new Exception("Error");
            }
        }
    }
}
