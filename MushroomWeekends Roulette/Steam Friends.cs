﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MushroomWeekends_Roulette
{
    public partial class Steam_Friends : Form
    {
        List<string> id64;
        List<Player> friendsList;
        public List<Player> checkedFriends;

        public Steam_Friends()
        {
            InitializeComponent();
            friendsList=new List<Player>();
    }

        private void loadFriends_Click(object sender, EventArgs e)
        {
            loadSteamFriends();
        }
        private void loadSteamFriends()
        {
            if (steamID.Text == "")
                return;

            friendsGrid.Rows.Clear();
            friendsList.Clear();
            string url_profile = "http://steamcommunity.com/id/" + steamID.Text + "/friends?xml=1";
            if (!GetFriendList(url_profile))
            {
                MessageBox.Show("Enter public steamID.", "This profile is private or not found!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!GetFriendInfo())
                return;
            if (!FillTable())
                return;
        }
        private bool GetFriendList(string url)
        {
            id64 = new List<string>();
            var reader = XmlReader.Create(url, new XmlReaderSettings { DtdProcessing = DtdProcessing.Parse});
            string id64_owner = "";
            bool changedUrl = false;
            try
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            switch (reader.Name)
                            {
                                case "error":
                                    if (changedUrl)
                                    {
                                        return false;
                                    }
                                    reader = XmlReader.Create("http://steamcommunity.com/profiles/" + steamID.Text + "/?xml=1");
                                    changedUrl = true;
                                    break;
                                case "steamID64":
                                    reader.Read();
                                    id64_owner = reader.Value;
                                    break;
                                case "friend":
                                    reader.Read();
                                    id64.Add(reader.Value);
                                    break;
                                case "steamID":
                                    reader.Read();
                                    friendsList.Add(new Player(reader.Value, null, id64_owner, true));
                                    break;

                            }
                            break;
                    }
                }
            }
            catch(Exception e)
            {
                return false;
            }
            return true;

        }

        private bool GetFriendInfo()
        {
            try
            {
                string name = "", id = "";
                Bitmap img = null;
                for (int i = 0; i < id64.Count; i++)
                {
                    friendlistLabel.Text = "Loading friend (" + (i + 1) + "/" + id64.Count + ")";
                    this.Refresh();
                    string profile_url = "http://steamcommunity.com/profiles/" + id64[i] + "?xml=1";
                    var reader = XmlReader.Create(profile_url, new XmlReaderSettings { DtdProcessing = DtdProcessing.Parse });
                    id = id64[i];
                    bool isPrivate = false;
                    bool fullInfo = false;
                    while (reader.Read() && !isPrivate && !fullInfo)
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                switch (reader.Name)
                                {
                                    case "steamID":
                                        reader.Read();
                                        name = reader.Value;
                                        break;
                                    case "visibilityState":
                                        reader.Read();
                                        if (reader.Value != "3")
                                        {
                                            isPrivate = true;
                                            fullInfo = true;
                                        }
                                        break;
                                    case "avatarIcon":
                                        reader.Read();
                                        img = GetAvatar(reader.Value);
                                        fullInfo = true;
                                        break;

                                }
                                break;
                        }
                    }
                    if (!isPrivate)
                        friendsList.Add(new Player(name, img, id, true));
                }
                friendlistLabel.Text = "Friend-list";
                return true;
            }
            catch(Exception)
            {
                MessageBox.Show("Something go wrong.");
                friendlistLabel.Text = "Friend-list";
                return false;
            }
        }
        private Bitmap GetAvatar(string url)
        {
            Bitmap dst;
            System.Net.WebRequest request = System.Net.WebRequest.Create(url);
            System.Net.WebResponse response = request.GetResponse();
            System.IO.Stream responseStream = response.GetResponseStream();
            dst = new Bitmap(responseStream);
            return dst;
        }

        private bool FillTable()
        {
            friendsGrid.Rows.Clear();
            for (int i = 1; i < friendsList.Count; i++)
            {
                friendsGrid.Rows.Add();
                friendsGrid.Rows[i - 1].Cells[0].Value = false;
                friendsGrid.Rows[i - 1].Cells[1].Value = friendsList[i].Image;
                friendsGrid.Rows[i - 1].Cells[2].Value = friendsList[i].Name;
            }
            friendsGrid.Focus();
            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkedFriends = new List<Player>();
            checkedFriends.Add(friendsList[0]);
            for (int i = 0; i < friendsGrid.Rows.Count; i++)
            {
                if ((bool)friendsGrid.Rows[i].Cells[0].Value)
                {
                    checkedFriends.Add(friendsList[i + 1]);
                }
            }
            if (checkedFriends.Count <= 1)
            {
                MessageBox.Show("Check some friend.");
                return;
            }
            this.DialogResult = DialogResult.OK;
        }

        private void friendsGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (friendsGrid.Rows.Count <= 0)
                button1.Enabled = false;
            else
                button1.Enabled = true;
        }

        private void steamID_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                loadSteamFriends();
            }
        }
    }
}
