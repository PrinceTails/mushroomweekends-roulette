﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace MushroomWeekends_Roulette
{
    public partial class Main : Form
    {
        List<string> games;
        List<Player> playerslist;
        public AddPlayerForm apf;
        bool remove_after_choice;
        public Main()
        {
            SetBrowserFeatureControl();
            InitializeComponent();
            playerslist = new List<Player>();
            remove_after_choice = false;
        }


        private void randomiseGameButton_Click(object sender, EventArgs e)
        {
            string startUrl = "http://wheeldecide.com/e.php?width=250&height=250&scrolling=no&frameborder=0";
            if (remove_after_choice)
                startUrl += "&remove=1";
            for(int i=0;i<gameListBox.Rows.Count;i++)
            {
                startUrl += "&c" + (i + 1) + "=" + gameListBox.Rows[i].Cells[0].Value;
            }
            wheelDecide.Navigate(startUrl);
        }

        private void loadGameListButton_Click(object sender, EventArgs e)
        {
            try
            {
                if(playerslist.Count<=0)
                {
                    MessageBox.Show("Players-list is empty.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                gameListBox.Rows.Clear();
                gameListBox.Columns.Clear();
                progressLoading.Value = 0;
                string url;
                int progressBarCoef= (int)50/playerslist.Count;
                for (int i = 0; i < playerslist.Count; i++)
                {
                    url = "http://steamcommunity.com/profiles/"+playerslist[i].Id64+"/games?xml=1";
                    statusLoading.Text = "Loading " + playerslist[i].Name + " games list.";
                    progressLoading.Value += progressBarCoef;
                    this.Refresh();
                    Thread.Sleep(500);
                    playerslist[i].ParseData(url);
                }

                List<Game> tmp = new List<Game>();

                statusLoading.Text = "Compare games list.";
                progressLoading.Value = 50;
                this.Refresh();
                Thread.Sleep(500);
                games = new List<string>();
                for (int k = 0; k < playerslist[0].ListOfGames.Count; k++)
                {
                    if (compareGames(1, playerslist[0].ListOfGames[k].Name))
                        tmp.Add(playerslist[0].ListOfGames[k]);
                }

                progressLoading.Value = 75;
                this.Refresh();
                Thread.Sleep(500);
                if (onlyInstalledBox.Checked)
                {
                    onlyInstalledGamesFilter(ref tmp);
                }
                for (int i=0; i<tmp.Count;i++)
                {
                    statusLoading.Text = "Filtering games list. ("+(i+1)+"/" + tmp.Count + ")";
                    this.Refresh();
                    Thread.Sleep(500);

                    if (FilterGame(tmp[i].AppId))
                        games.Add(tmp[i].Name);
                }


                gameListBox.Columns.Add("name","Name");
                gameListBox.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                for (int i = 0; i < games.Count; i++)
                    gameListBox.Rows.Add(games[i].ToString());

                statusLoading.Text = "Ready to spin!";
                progressLoading.Value = 100;

                randomiseGameButton.Enabled = true;
                
                gameListBox.Focus();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        public bool compareGames(int id, string name)
        {
            for(int i=0; i< playerslist[id].ListOfGames.Count;i++)
            {
                if(playerslist[id].ListOfGames[i].Name==name)
                {
                    if (id == playerslist.Count - 1)
                        return true;
                    else
                        return compareGames(id+1,name);
                }
            }
            return false;
        }

        private void onlyInstalledGamesFilter(ref List<Game> l)
        {
            List<Game> tmp = new List<Game>();
            OS_info();
            List<FileInfo> appList = getList();

            List<string> installed = ArraySteam(appList);
            for(int i=0;i<l.Count;i++)
                for(int j=0;j<installed.Count;j++)
                {
                    if (l[i].Name == installed[j])
                        tmp.Add(l[i]);
                }

            l = tmp;
        }

        private bool FilterGame(int id)
        {
            int cur = 0;
            int readed = 0;
            string resp = "";
            string webSite = String.Format(@"http://steamdb.info/app/{0}/info", id);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(webSite);
            request.UserAgent = "MushroomWeekends by Prince Tails";
            request.Accept = "text/html";
            HttpWebResponse wRes = (HttpWebResponse)request.GetResponse();

            var da = wRes.GetResponseStream();
            do
            {
                byte[] buf = new byte[400];
                readed = da.Read(buf, 0, 400);
                cur += readed;
                if (cur > 8000)
                    resp += Encoding.UTF8.GetString(buf, 0, buf.Length);
            }
            while (cur <= 16500);
            wRes.Close();
            string regex2 = "\"applicationCategory\">(.*?)</td>";
            MatchCollection match2 = Regex.Matches(resp, regex2);
            foreach (Match m2 in match2)
            {
                if (m2.Groups[1].Value == "Game")
                {
                    string regex = "aria-label\\s*=\\s*(?:[\"'](?<1>[^\"']*)[\"']|(?<1>\\S+))";
                    MatchCollection match = Regex.Matches(resp, regex);
                    foreach (Match m in match)
                    {
                        if (m.Groups[1].Value == "Multi-player" || m.Groups[1].Value == "Co-op" || m.Groups[1].Value == "Cross-Platform Multiplayer" || m.Groups[1].Value== "Online Multi-Player")
                        {
                            return true;
                        }
                    }
                }
            }
            
            return false;
        }


        #region browser settings
        private void SetBrowserFeatureControlKey(string feature, string appName, uint value)
        {
            using (var key = Registry.CurrentUser.CreateSubKey(
                String.Concat(@"Software\Microsoft\Internet Explorer\Main\FeatureControl\", feature),
                RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                key.SetValue(appName, (UInt32)value, RegistryValueKind.DWord);
            }
        }
        private void SetBrowserFeatureControl()
        {
            // http://msdn.microsoft.com/en-us/library/ee330720(v=vs.85).aspx

            // FeatureControl settings are per-process
            var fileName = System.IO.Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);

            // make the control is not running inside Visual Studio Designer
            if (String.Compare(fileName, "devenv.exe", true) == 0 || String.Compare(fileName, "XDesProc.exe", true) == 0)
                return;

            SetBrowserFeatureControlKey("FEATURE_BROWSER_EMULATION", fileName, GetBrowserEmulationMode()); // Webpages containing standards-based !DOCTYPE directives are displayed in IE10 Standards mode.
            SetBrowserFeatureControlKey("FEATURE_AJAX_CONNECTIONEVENTS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_ENABLE_CLIPCHILDREN_OPTIMIZATION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_MANAGE_SCRIPT_CIRCULAR_REFS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_DOMSTORAGE ", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_GPU_RENDERING ", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_IVIEWOBJECTDRAW_DMLT9_WITH_GDI  ", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_DISABLE_LEGACY_COMPRESSION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_LOCALMACHINE_LOCKDOWN", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_OBJECT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_SCRIPT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_DISABLE_NAVIGATION_SOUNDS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_SCRIPTURL_MITIGATION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_SPELLCHECKING", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_STATUS_BAR_THROTTLING", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_TABBED_BROWSING", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_VALIDATE_NAVIGATE_URL", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_DOCUMENT_ZOOM", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_POPUPMANAGEMENT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_MOVESIZECHILD", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_ADDON_MANAGEMENT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_WEBSOCKET", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WINDOW_RESTRICTIONS ", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_XMLHTTP", fileName, 1);
        }

        private UInt32 GetBrowserEmulationMode()
        {
            int browserVersion = 7;
            using (var ieKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer",
                RegistryKeyPermissionCheck.ReadSubTree,
                System.Security.AccessControl.RegistryRights.QueryValues))
            {
                var version = ieKey.GetValue("svcVersion");
                if (null == version)
                {
                    version = ieKey.GetValue("Version");
                    if (null == version)
                        throw new ApplicationException("Microsoft Internet Explorer is required!");
                }
                int.TryParse(version.ToString().Split('.')[0], out browserVersion);
            }

            UInt32 mode = 11000; // Internet Explorer 11. Webpages containing standards-based !DOCTYPE directives are displayed in IE11 Standards mode. Default value for Internet Explorer 11.
            switch (browserVersion)
            {
                case 7:
                    mode = 7000; // Webpages containing standards-based !DOCTYPE directives are displayed in IE7 Standards mode. Default value for applications hosting the WebBrowser Control.
                    break;
                case 8:
                    mode = 8000; // Webpages containing standards-based !DOCTYPE directives are displayed in IE8 mode. Default value for Internet Explorer 8
                    break;
                case 9:
                    mode = 9000; // Internet Explorer 9. Webpages containing standards-based !DOCTYPE directives are displayed in IE9 mode. Default value for Internet Explorer 9.
                    break;
                case 10:
                    mode = 10000; // Internet Explorer 10. Webpages containing standards-based !DOCTYPE directives are displayed in IE10 mode. Default value for Internet Explorer 10.
                    break;
                default:
                    // use IE11 mode by default
                    break;
            }

            return mode;
        }
        #endregion

        private void customNames_CheckedChanged(object sender, EventArgs e)
        {

            if (customNames.Checked)
            {
                if (gameListBox.Columns.Count > 0)
                {
                    if (MessageBox.Show("Games List will be cleared.\nConfirm?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        gameListBox.Columns.Clear();
                    else
                    {
                        customNames.Checked = false;
                        return;
                    }
                }
                else
                {
                    gameListBox.Columns.Add("name", "Name");
                    gameListBox.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
            else
            {
                if (gameListBox.Columns.Count > 0)
                {
                    if (MessageBox.Show("Games List will be cleared.\nConfirm?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        gameListBox.Columns.Clear();
                    else
                        return;
                }
                else
                {
                    gameListBox.Columns.Clear();
                    randomiseGameButton.Enabled = false;
                }
            }

            vieweditButton.Enabled = !customNames.Checked;
            loadGameListButton.Enabled = !customNames.Checked;
            onlyInstalledBox.Enabled = !customNames.Checked;
        }

        private void gameListBox_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (gameListBox.Rows.Count > 2)
                randomiseGameButton.Enabled = true;
            else
                randomiseGameButton.Enabled = false;
            totalgamesLabel.Text = "Total games: " + (gameListBox.Rows.Count-1);
        }

        #region filter installed games
        string os_path = "";

        private void OS_info()
        {
            if (Environment.Is64BitOperatingSystem)
                os_path = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Steam";
            else
                os_path = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\Steam";
        }


        private string[] get_libraryfolders()
        {
            string[] lf;
            char[] split_char = { '"', '}', '{', '\n', '\t' };
            StreamReader stream = new StreamReader(os_path + @"\steamapps\libraryfolders.vdf");
            string text = stream.ReadToEnd();
            lf = text.Split(split_char, StringSplitOptions.RemoveEmptyEntries);
            lf[4] = os_path;
            return lf;
        }

        private List<FileInfo> getList()
        {
            List<FileInfo> manifest_list = new List<FileInfo>();

            string[] lf = get_libraryfolders();
            for (int j = 4; j < lf.GetLength(0); j += 2)
            {

                DirectoryInfo dir = new DirectoryInfo(lf[j] + @"\SteamApps");

                FileInfo[] finfo = dir.GetFiles();
                for (int i = 0; i < finfo.GetLength(0); i++)
                {
                    string ch = Path.GetFileNameWithoutExtension(finfo[i].FullName);
                    if (ch.Contains("appmanifest"))
                        manifest_list.Add(finfo[i]);
                }
            }
            return manifest_list;
        }

        private List<string> ArraySteam(List<FileInfo> manifest_list)
        {
            List<string> app = new List<string>();
            char[] split_char = { '"', '}', '{', '\n', '\t' };

            for (int i = 0; i < manifest_list.Count; i++)
            {
                string name = "";

                byte[] buffer = new byte[128];

                //открытие и считывания данных из манифеста
                FileStream fs = manifest_list[i].OpenRead();
                fs.Read(buffer, 0, 128);
                fs.Close();

                string manifest_descriptor = Encoding.UTF8.GetString(buffer);
                string[] headers = manifest_descriptor.Split(split_char, StringSplitOptions.RemoveEmptyEntries);

                for (int j = 0; j < headers.Length; j++)
                {
                    if (name == "" && headers[j] == "name")
                    {
                        name = headers[j + 1];
                        break;
                    }
                }
                app.Add(name);
            }
            return app;
        }
        #endregion

        private void vieweditButton_Click(object sender, EventArgs e)
        {
            if (apf == null)
            {
                apf = new AddPlayerForm();
            }
            if(apf.ShowDialog()==DialogResult.OK)
            {
                playerslist = new List<Player>();
                for(int i=0;i<apf.playersList.Items.Count;i++)
                {
                    if (apf.listFromForm.Count != 0 && i <= apf.listFromForm.Count - 1)
                        playerslist.Add(apf.listFromForm[i]);
                    if (apf.listFromSteam.Count != 0 && i <= apf.listFromSteam.Count - 1)
                        playerslist.Add(apf.listFromSteam[i]);
                }
                totalLabel.Text = "Total players: " + playerslist.Count.ToString();
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (apf != null)
            {
                apf.Close();
                apf.Dispose();
            }
        }

        private void remaftchBox_CheckedChanged(object sender, EventArgs e)
        {
            remove_after_choice = remaftchBox.Checked;
        }

        private void rpsButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Comming soon", "Sorry!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
            if (rpsRunning())
            {
                //добавить автоматическое переключение на приложение, если оно открыто
                MessageBox.Show("The application is already open","Information",MessageBoxButtons.OK,MessageBoxIcon.Information);
                return;
            }
            if (File.Exists("rps.exe"))
                Process.Start("rps.exe");
        }

        private bool rpsRunning()
        {
            Process[] proc = Process.GetProcesses();
            for (int i = 0; i < proc.Length; i++)
                if (proc[i].ProcessName == "rps")
                    return true;
            return false;
        }
    }
}
