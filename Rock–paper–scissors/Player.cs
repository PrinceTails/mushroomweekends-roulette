﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Rock_paper_scissors
{
    public class Player
    {
        public string Name { get; set; }
        public bool IsServer { get; set; }
        public TcpClient tcpClient { get; set; }

        public Player(string name)
        {
            Name = name;
        }

        public Player(string name, TcpClient tcp)
        {
            Name = name;
            tcpClient = tcp;
            //IsServer = isserver;
        }
    }
}
