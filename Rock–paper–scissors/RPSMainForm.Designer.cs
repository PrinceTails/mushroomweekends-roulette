﻿namespace Rock_paper_scissors
{
    partial class RPSMainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gameControl = new System.Windows.Forms.TabControl();
            this.serverPage = new System.Windows.Forms.TabPage();
            this.splitContainerServer = new System.Windows.Forms.SplitContainer();
            this.disableServerButton = new System.Windows.Forms.Button();
            this.createServerButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.playerNameServerBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.copyServerIpButton = new System.Windows.Forms.Button();
            this.refreshServerIpButton = new System.Windows.Forms.Button();
            this.serverIplabel = new System.Windows.Forms.Label();
            this.infoStringLabel = new System.Windows.Forms.Label();
            this.playersSummaryGrid = new System.Windows.Forms.DataGridView();
            this.nameDataGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pointsDataGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scissorsPicture = new System.Windows.Forms.PictureBox();
            this.paperPicture = new System.Windows.Forms.PictureBox();
            this.rockPicture = new System.Windows.Forms.PictureBox();
            this.startGameButton = new System.Windows.Forms.Button();
            this.clientPage = new System.Windows.Forms.TabPage();
            this.splitContainerClient = new System.Windows.Forms.SplitContainer();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.playerNameBox = new System.Windows.Forms.TextBox();
            this.connectToServerButton = new System.Windows.Forms.Button();
            this.serverIpBox = new System.Windows.Forms.TextBox();
            this.timerConnections = new System.Windows.Forms.Timer(this.components);
            this.gameControl.SuspendLayout();
            this.serverPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerServer)).BeginInit();
            this.splitContainerServer.Panel1.SuspendLayout();
            this.splitContainerServer.Panel2.SuspendLayout();
            this.splitContainerServer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playersSummaryGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scissorsPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paperPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rockPicture)).BeginInit();
            this.clientPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerClient)).BeginInit();
            this.splitContainerClient.Panel1.SuspendLayout();
            this.splitContainerClient.SuspendLayout();
            this.SuspendLayout();
            // 
            // gameControl
            // 
            this.gameControl.Controls.Add(this.serverPage);
            this.gameControl.Controls.Add(this.clientPage);
            this.gameControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gameControl.Location = new System.Drawing.Point(0, 0);
            this.gameControl.Name = "gameControl";
            this.gameControl.SelectedIndex = 0;
            this.gameControl.Size = new System.Drawing.Size(677, 379);
            this.gameControl.TabIndex = 0;
            // 
            // serverPage
            // 
            this.serverPage.Controls.Add(this.splitContainerServer);
            this.serverPage.Location = new System.Drawing.Point(4, 22);
            this.serverPage.Name = "serverPage";
            this.serverPage.Padding = new System.Windows.Forms.Padding(3);
            this.serverPage.Size = new System.Drawing.Size(669, 353);
            this.serverPage.TabIndex = 0;
            this.serverPage.Text = "Server";
            this.serverPage.UseVisualStyleBackColor = true;
            // 
            // splitContainerServer
            // 
            this.splitContainerServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerServer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainerServer.IsSplitterFixed = true;
            this.splitContainerServer.Location = new System.Drawing.Point(3, 3);
            this.splitContainerServer.Name = "splitContainerServer";
            // 
            // splitContainerServer.Panel1
            // 
            this.splitContainerServer.Panel1.Controls.Add(this.disableServerButton);
            this.splitContainerServer.Panel1.Controls.Add(this.createServerButton);
            this.splitContainerServer.Panel1.Controls.Add(this.label4);
            this.splitContainerServer.Panel1.Controls.Add(this.playerNameServerBox);
            this.splitContainerServer.Panel1.Controls.Add(this.label1);
            this.splitContainerServer.Panel1.Controls.Add(this.numericUpDown1);
            this.splitContainerServer.Panel1.Controls.Add(this.copyServerIpButton);
            this.splitContainerServer.Panel1.Controls.Add(this.refreshServerIpButton);
            this.splitContainerServer.Panel1.Controls.Add(this.serverIplabel);
            // 
            // splitContainerServer.Panel2
            // 
            this.splitContainerServer.Panel2.Controls.Add(this.infoStringLabel);
            this.splitContainerServer.Panel2.Controls.Add(this.playersSummaryGrid);
            this.splitContainerServer.Panel2.Controls.Add(this.scissorsPicture);
            this.splitContainerServer.Panel2.Controls.Add(this.paperPicture);
            this.splitContainerServer.Panel2.Controls.Add(this.rockPicture);
            this.splitContainerServer.Panel2.Controls.Add(this.startGameButton);
            this.splitContainerServer.Panel2.Enabled = false;
            this.splitContainerServer.Size = new System.Drawing.Size(663, 347);
            this.splitContainerServer.SplitterDistance = 115;
            this.splitContainerServer.TabIndex = 3;
            // 
            // disableServerButton
            // 
            this.disableServerButton.Enabled = false;
            this.disableServerButton.Location = new System.Drawing.Point(5, 276);
            this.disableServerButton.Name = "disableServerButton";
            this.disableServerButton.Size = new System.Drawing.Size(107, 23);
            this.disableServerButton.TabIndex = 8;
            this.disableServerButton.Text = "DIsable server";
            this.disableServerButton.UseVisualStyleBackColor = true;
            this.disableServerButton.Click += new System.EventHandler(this.disableServerButton_Click);
            // 
            // createServerButton
            // 
            this.createServerButton.Location = new System.Drawing.Point(5, 247);
            this.createServerButton.Name = "createServerButton";
            this.createServerButton.Size = new System.Drawing.Size(107, 23);
            this.createServerButton.TabIndex = 7;
            this.createServerButton.Text = "Create server";
            this.createServerButton.UseVisualStyleBackColor = true;
            this.createServerButton.Click += new System.EventHandler(this.createServerButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Your name";
            // 
            // playerNameServerBox
            // 
            this.playerNameServerBox.Location = new System.Drawing.Point(5, 54);
            this.playerNameServerBox.MaxLength = 15;
            this.playerNameServerBox.Name = "playerNameServerBox";
            this.playerNameServerBox.Size = new System.Drawing.Size(107, 20);
            this.playerNameServerBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 189);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Points to win";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(20, 207);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(82, 20);
            this.numericUpDown1.TabIndex = 3;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // copyServerIpButton
            // 
            this.copyServerIpButton.Location = new System.Drawing.Point(5, 130);
            this.copyServerIpButton.Name = "copyServerIpButton";
            this.copyServerIpButton.Size = new System.Drawing.Size(107, 23);
            this.copyServerIpButton.TabIndex = 1;
            this.copyServerIpButton.Text = "Copy IP";
            this.copyServerIpButton.UseVisualStyleBackColor = true;
            this.copyServerIpButton.Click += new System.EventHandler(this.copyServerIpButton_Click);
            // 
            // refreshServerIpButton
            // 
            this.refreshServerIpButton.Location = new System.Drawing.Point(5, 159);
            this.refreshServerIpButton.Name = "refreshServerIpButton";
            this.refreshServerIpButton.Size = new System.Drawing.Size(107, 23);
            this.refreshServerIpButton.TabIndex = 2;
            this.refreshServerIpButton.Text = "Refresh IP";
            this.refreshServerIpButton.UseVisualStyleBackColor = true;
            this.refreshServerIpButton.Click += new System.EventHandler(this.refreshServerIpButton_Click);
            // 
            // serverIplabel
            // 
            this.serverIplabel.AutoSize = true;
            this.serverIplabel.Location = new System.Drawing.Point(2, 101);
            this.serverIplabel.Name = "serverIplabel";
            this.serverIplabel.Size = new System.Drawing.Size(88, 26);
            this.serverIplabel.TabIndex = 0;
            this.serverIplabel.Text = "Your IP: \r\n255.255.255.255";
            // 
            // infoStringLabel
            // 
            this.infoStringLabel.AutoSize = true;
            this.infoStringLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.infoStringLabel.Location = new System.Drawing.Point(22, 13);
            this.infoStringLabel.Name = "infoStringLabel";
            this.infoStringLabel.Size = new System.Drawing.Size(195, 25);
            this.infoStringLabel.TabIndex = 5;
            this.infoStringLabel.Text = "Information string";
            this.infoStringLabel.Visible = false;
            // 
            // playersSummaryGrid
            // 
            this.playersSummaryGrid.AllowUserToAddRows = false;
            this.playersSummaryGrid.AllowUserToDeleteRows = false;
            this.playersSummaryGrid.AllowUserToResizeColumns = false;
            this.playersSummaryGrid.AllowUserToResizeRows = false;
            this.playersSummaryGrid.BackgroundColor = System.Drawing.Color.White;
            this.playersSummaryGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.playersSummaryGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGrid,
            this.pointsDataGrid});
            this.playersSummaryGrid.Location = new System.Drawing.Point(58, 219);
            this.playersSummaryGrid.MultiSelect = false;
            this.playersSummaryGrid.Name = "playersSummaryGrid";
            this.playersSummaryGrid.ReadOnly = true;
            this.playersSummaryGrid.RowHeadersVisible = false;
            this.playersSummaryGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.playersSummaryGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.playersSummaryGrid.Size = new System.Drawing.Size(196, 111);
            this.playersSummaryGrid.TabIndex = 4;
            // 
            // nameDataGrid
            // 
            this.nameDataGrid.HeaderText = "Name";
            this.nameDataGrid.MaxInputLength = 15;
            this.nameDataGrid.Name = "nameDataGrid";
            this.nameDataGrid.ReadOnly = true;
            this.nameDataGrid.Width = 150;
            // 
            // pointsDataGrid
            // 
            this.pointsDataGrid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.pointsDataGrid.HeaderText = "Points";
            this.pointsDataGrid.MaxInputLength = 1;
            this.pointsDataGrid.Name = "pointsDataGrid";
            this.pointsDataGrid.ReadOnly = true;
            this.pointsDataGrid.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.pointsDataGrid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.pointsDataGrid.Width = 42;
            // 
            // scissorsPicture
            // 
            this.scissorsPicture.Image = global::Rock_paper_scissors.Properties.Resources.scissors;
            this.scissorsPicture.Location = new System.Drawing.Point(355, 82);
            this.scissorsPicture.MaximumSize = new System.Drawing.Size(100, 100);
            this.scissorsPicture.MinimumSize = new System.Drawing.Size(100, 100);
            this.scissorsPicture.Name = "scissorsPicture";
            this.scissorsPicture.Size = new System.Drawing.Size(100, 100);
            this.scissorsPicture.TabIndex = 3;
            this.scissorsPicture.TabStop = false;
            this.scissorsPicture.Visible = false;
            // 
            // paperPicture
            // 
            this.paperPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.paperPicture.Image = global::Rock_paper_scissors.Properties.Resources.paper;
            this.paperPicture.Location = new System.Drawing.Point(211, 82);
            this.paperPicture.MaximumSize = new System.Drawing.Size(100, 100);
            this.paperPicture.MinimumSize = new System.Drawing.Size(100, 100);
            this.paperPicture.Name = "paperPicture";
            this.paperPicture.Size = new System.Drawing.Size(100, 100);
            this.paperPicture.TabIndex = 2;
            this.paperPicture.TabStop = false;
            this.paperPicture.Visible = false;
            // 
            // rockPicture
            // 
            this.rockPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.rockPicture.Image = global::Rock_paper_scissors.Properties.Resources.rock;
            this.rockPicture.Location = new System.Drawing.Point(57, 82);
            this.rockPicture.MaximumSize = new System.Drawing.Size(100, 100);
            this.rockPicture.MinimumSize = new System.Drawing.Size(100, 100);
            this.rockPicture.Name = "rockPicture";
            this.rockPicture.Size = new System.Drawing.Size(100, 100);
            this.rockPicture.TabIndex = 1;
            this.rockPicture.TabStop = false;
            this.rockPicture.Visible = false;
            // 
            // startGameButton
            // 
            this.startGameButton.Location = new System.Drawing.Point(355, 307);
            this.startGameButton.Name = "startGameButton";
            this.startGameButton.Size = new System.Drawing.Size(75, 23);
            this.startGameButton.TabIndex = 0;
            this.startGameButton.Text = "Start Game";
            this.startGameButton.UseVisualStyleBackColor = true;
            this.startGameButton.Click += new System.EventHandler(this.startGameButton_Click);
            // 
            // clientPage
            // 
            this.clientPage.Controls.Add(this.splitContainerClient);
            this.clientPage.Location = new System.Drawing.Point(4, 22);
            this.clientPage.Name = "clientPage";
            this.clientPage.Padding = new System.Windows.Forms.Padding(3);
            this.clientPage.Size = new System.Drawing.Size(669, 353);
            this.clientPage.TabIndex = 1;
            this.clientPage.Text = "Client";
            this.clientPage.UseVisualStyleBackColor = true;
            // 
            // splitContainerClient
            // 
            this.splitContainerClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerClient.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainerClient.IsSplitterFixed = true;
            this.splitContainerClient.Location = new System.Drawing.Point(3, 3);
            this.splitContainerClient.Name = "splitContainerClient";
            // 
            // splitContainerClient.Panel1
            // 
            this.splitContainerClient.Panel1.Controls.Add(this.label3);
            this.splitContainerClient.Panel1.Controls.Add(this.label2);
            this.splitContainerClient.Panel1.Controls.Add(this.playerNameBox);
            this.splitContainerClient.Panel1.Controls.Add(this.connectToServerButton);
            this.splitContainerClient.Panel1.Controls.Add(this.serverIpBox);
            // 
            // splitContainerClient.Panel2
            // 
            this.splitContainerClient.Panel2.Enabled = false;
            this.splitContainerClient.Size = new System.Drawing.Size(663, 347);
            this.splitContainerClient.SplitterDistance = 115;
            this.splitContainerClient.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Server IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Your name";
            // 
            // playerNameBox
            // 
            this.playerNameBox.Location = new System.Drawing.Point(5, 53);
            this.playerNameBox.MaxLength = 15;
            this.playerNameBox.Name = "playerNameBox";
            this.playerNameBox.Size = new System.Drawing.Size(107, 20);
            this.playerNameBox.TabIndex = 2;
            // 
            // connectToServerButton
            // 
            this.connectToServerButton.Location = new System.Drawing.Point(5, 122);
            this.connectToServerButton.Name = "connectToServerButton";
            this.connectToServerButton.Size = new System.Drawing.Size(107, 23);
            this.connectToServerButton.TabIndex = 1;
            this.connectToServerButton.Text = "Connect";
            this.connectToServerButton.UseVisualStyleBackColor = true;
            this.connectToServerButton.Click += new System.EventHandler(this.connectToServerButton_Click);
            // 
            // serverIpBox
            // 
            this.serverIpBox.Location = new System.Drawing.Point(5, 96);
            this.serverIpBox.Name = "serverIpBox";
            this.serverIpBox.Size = new System.Drawing.Size(107, 20);
            this.serverIpBox.TabIndex = 0;
            // 
            // timerConnections
            // 
            this.timerConnections.Tick += new System.EventHandler(this.timerConnections_Tick);
            // 
            // RPSMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(677, 379);
            this.Controls.Add(this.gameControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "RPSMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rock–paper–scissors";
            this.gameControl.ResumeLayout(false);
            this.serverPage.ResumeLayout(false);
            this.splitContainerServer.Panel1.ResumeLayout(false);
            this.splitContainerServer.Panel1.PerformLayout();
            this.splitContainerServer.Panel2.ResumeLayout(false);
            this.splitContainerServer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerServer)).EndInit();
            this.splitContainerServer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playersSummaryGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scissorsPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paperPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rockPicture)).EndInit();
            this.clientPage.ResumeLayout(false);
            this.splitContainerClient.Panel1.ResumeLayout(false);
            this.splitContainerClient.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerClient)).EndInit();
            this.splitContainerClient.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl gameControl;
        private System.Windows.Forms.TabPage serverPage;
        private System.Windows.Forms.TabPage clientPage;
        private System.Windows.Forms.SplitContainer splitContainerClient;
        private System.Windows.Forms.TextBox playerNameBox;
        private System.Windows.Forms.Button connectToServerButton;
        private System.Windows.Forms.TextBox serverIpBox;
        private System.Windows.Forms.Button refreshServerIpButton;
        private System.Windows.Forms.Button copyServerIpButton;
        private System.Windows.Forms.Label serverIplabel;
        private System.Windows.Forms.SplitContainer splitContainerServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox playerNameServerBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button createServerButton;
        private System.Windows.Forms.PictureBox scissorsPicture;
        private System.Windows.Forms.PictureBox paperPicture;
        private System.Windows.Forms.PictureBox rockPicture;
        private System.Windows.Forms.Button startGameButton;
        private System.Windows.Forms.DataGridView playersSummaryGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn pointsDataGrid;
        private System.Windows.Forms.Button disableServerButton;
        private System.Windows.Forms.Label infoStringLabel;
        private System.Windows.Forms.Timer timerConnections;
    }
}

