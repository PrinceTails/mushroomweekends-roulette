﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Text.RegularExpressions;

namespace Rock_paper_scissors
{
    public partial class RPSMainForm : Form
    {
        string serverip = "";
        Player client;
        List <Player> clientList;
        Server server;
        int clientCount = 0;
        public RPSMainForm()
        {
            InitializeComponent();
            getServerIp();

        }
        #region Client Part
        private void connectToServerButton_Click(object sender, EventArgs e)
        {
            IPAddress ipServer;
            if(playerNameBox.Text=="")
            {
                MessageBox.Show("Enter your name", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(!IPAddress.TryParse(serverIpBox.Text,out ipServer))
            {
                MessageBox.Show("Incorrect IP-adress", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            client = new Player(playerNameBox.Text);
            splitContainerServer.Enabled = false;
            Client clt = new Client(playerNameBox.Text, new IPEndPoint(ipServer,3334));
            clt.Start();
            
        }
        #endregion
        #region Server Part
        private void getServerIp()
        {
            try
            {
                WebClient client = new WebClient();
                string externalIP = client.DownloadString("http://icanhazip.com/");
                serverIplabel.Text = "Your IP: \n" + externalIP;
                serverip = externalIP;
            }
            catch { serverIplabel.Text = "Your IP: \n not found"; }
        }

        private void refreshServerIpButton_Click(object sender, EventArgs e)
        {
            getServerIp();
        }

        private void copyServerIpButton_Click(object sender, EventArgs e)
        {
            if(serverip=="")
            {
                MessageBox.Show("Ip not found");
            }

            Clipboard.SetText(serverip);
        }

        private void createServerButton_Click(object sender, EventArgs e)
        {
            if (playerNameServerBox.Text == "")
            {
                MessageBox.Show("Enter your name", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            clientList = new List<Player>();
            //clientList.Add(new Player(playerNameServerBox.Text));
            splitContainerClient.Enabled = false;

            splitContainerServer.Panel2.Enabled = true;
            disableServerButton.Enabled = true;
            createServerButton.Enabled = false;

            //creating server
            server = new Server();
            server.Start();

            timerConnections.Start();

            infoStringLabel.Visible = true;
            infoStringLabel.Text = "Waiting connections...";

        }

        private void startGameButton_Click(object sender, EventArgs e)
        {
            if(clientList.Count<2)
            {
                MessageBox.Show("Not enought players.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            rockPicture.Visible = true;
            paperPicture.Visible = true;
            scissorsPicture.Visible = true;
        }

        private void disableServerButton_Click(object sender, EventArgs e)
        {
            server.Stop();
            if (clientList.Count>0)
            {
                splitContainerServer.Panel2.Enabled = false;
                disableServerButton.Enabled = false;
                createServerButton.Enabled = true;
                infoStringLabel.Visible = false;
            }
            
        }


        #endregion

        private void timerConnections_Tick(object sender, EventArgs e)
        {
            if(server.clientsTable.Count>clientCount)
            {
                clientCount = server.clientsTable.Count;
                clientList = server.clientsTable;
                playersSummaryGrid.Rows.Clear();
                for (int i = 0; i < clientList.Count; i++)
                    playersSummaryGrid.Rows.Add(clientList[i].Name, 0);
                
            }
        }
    }
}
