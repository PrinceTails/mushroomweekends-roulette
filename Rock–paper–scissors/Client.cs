﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

namespace Rock_paper_scissors
{

    public class Client
    {
        private string myMessage = "";

        private TcpClient client = new TcpClient();
        NetworkStream clientStream;
        ASCIIEncoding encoder = new ASCIIEncoding();
        private IPEndPoint serverEndPoint;
        private string clientName;
        //private delegate void WriteMessageDelegate(string msg);
        //private delegate void UpdateLabelDelegate(Label lblCtrl, string msg);
        
        public Client(string name, IPEndPoint ipep)
        {
            serverEndPoint = ipep;
            clientName = name;
        }

        public void Start()
        {
            try
            {
                client.Connect(serverEndPoint);
            }
            catch (System.Net.Sockets.SocketException)
            {
                // TODO: Handle error
            }

            clientStream = client.GetStream();

            //Thread client_thread = new Thread(new ThreadStart(Run_client));
            Thread client_thread = new Thread(new ThreadStart(registerOnServer));
            client_thread.Start();

            //RequestClientId();
        }

        private void registerOnServer()
        {
            byte[] buffer = encoder.GetBytes("AUTO " + clientName.ToString());
            clientStream.Write(buffer, 0, buffer.Length);
        }

        //private void RequestClientId()
        //{
        //    int serverID = 999;
        //    //myMessage = Convert.ToString(serverID).PadLeft(3, '0');
        //    //myMessage = myMessage + "GetClientId";
        //    FormatMessage(serverID, "GetClientId");
        //    SendMessage();
        //}

        private void Run_client()
        {
            byte[] data = new byte[256];

            while (true)
            {
                do
                {
                    int bytecount = 0;
                    Array.Clear(data, 0, data.Length);
                    String responseData = String.Empty;

                    bytecount = clientStream.Read(data, 0, data.Length);
                    //responseData = encoder.GetString(data);

                    //if (responseData != String.Empty)
                    if (bytecount != 0)
                    {
                        HandleMessage(data);
                    }

                    //clientStream.Flush();

                } while (clientStream.DataAvailable);
            }
        }

        private void SendMessage()
        {
            byte[] buffer = encoder.GetBytes(myMessage);

            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();

            myMessage = "";
        }

        private void HandleMessage(byte[] data)
        {
            try
            {
                // TODO: This parsing to be moved to a Utils namespace
                int id = Convert.ToInt32(encoder.GetString(data, 0, 3));
                string msg = encoder.GetString(data, 3, data.Length - 3);

                if (id == 999)
                {
                    // Handle messages from server
                    // TODO Improve upon .contains check
                    if (msg.Contains("SetClientId"))
                    {
                        // Store integer for usage
                        myID = Convert.ToInt32(msg.Substring(11, 3));

                        // provide string to update label
                    }
                }
                else if (id == myID) // Only handle messages intended for myself
                {
                }
            }
            catch (System.FormatException)
            {
                // TODO : Handle error
            }
        }

        //private void ProcessMessage()
        //{
        //    if (txtDestinationClient.TextLength != 0 &&
        //        txtMessage.TextLength != 0)
        //    {
        //        // Format and send the messsage
        //        FormatMessage(Convert.ToInt32(txtDestinationClient.Text), txtMessage.Text);
        //        SendMessage();

        //        // Append the sent message to display box
        //        WriteMessage("Me >> Client #" + Convert.ToInt32(txtDestinationClient.Text) + " : " + txtMessage.Text);

        //        // Clear the textboxes and reset focus
        //        txtDestinationClient.Clear();
        //        txtMessage.Clear();
        //        txtDestinationClient.Focus();
        //    }
        //}

        private void FormatMessage(int id, string msg)
        {
            myMessage = id.ToString().PadLeft(3, '0') + msg;
        }
    }
}